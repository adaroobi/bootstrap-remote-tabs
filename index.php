<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="assets/plugins/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<div id='loadingDiv'>
    <div class="progress">
        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0"
             aria-valuemax="100" style="width: 0%">
            <span class="sr-only">80% Complete (danger)</span>
        </div>
    </div>
</div>
<div class="container" style="margin-top: 15em">
    <section>
        <article class="row">
            <div class="col-md-4 col-md-offset-4">
                <!-- TAB NAVIGATION -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Tab1</a></li>
                    <li><a href="content.php" role="tab" data-target="#tab2" data-toggle="remoteTab">Tab2</a></li>
                    <li><a href="content.php" data-post='{"data": "xxx"}' data-container="extra" role="tab" data-target="#tab3" data-toggle="remoteTab">Tab3</a></li>
                </ul>
                <!-- TAB CONTENT -->
                <div class="tab-content">
                    <div class="active tab-pane fade in" id="tab1">
                        <h2>Tab1</h2>
                        <p>Lorem ipsum.</p>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                        <h2>Tab2</h2>
                        <p>Lorem ipsum.</p>
                    </div>
                    <div class="tab-pane fade" id="tab3">
                        <h2>Tab3</h2>
                        <p>Lorem ipsum.</p>
                    </div>
                </div>

            </div>
        </article>
    </section>
</div>
<script src="assets/plugins/components/jquery/jquery.min.js"></script>
<script src="assets/plugins/twbs/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Remote Tabs Lib -->
<script src="assets/js/bs-remote-tabs.js"></script>
</body>
</html>