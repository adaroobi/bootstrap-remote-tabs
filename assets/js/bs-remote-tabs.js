/**
 * Created by adaroobi on 29/08/2016.
 */

var loadedTabs = [];

$('[data-toggle="remoteTab"]').click(function (e) {

    // fetch tab anchor for info.
    // todo maybe can load options through here..
    var $this = $(this),
        loadURL = $this.attr('href'),
        selectorID = $this.attr('data-container'), // data-container for #selectorID to make use of load..
        postData = $this.attr('data-post'),
        tabContainer = $this.attr('data-target');

    postData = postData ? JSON.parse(postData) : {};

    // check if tab is been loaded before..
    if (loadedTabs.indexOf(tabContainer) === -1) {

        // refer to jQuery API about .load()
        $(tabContainer).load(loadURL + (selectorID ? ' #' + selectorID : ''), postData, function (response, status, xhr) {

            var progressBar = $('.progress-bar');
            progressBar.animate({
                width: "100%"
            }, 250, function () {
                // rest progress bar to 0%
                progressBar.css('width', '0%');
                loadingDiv.hide(200);
            });

            // show the tab
            $this.tab('show');

            // mark as loaded tab
            loadedTabs.push(tabContainer);

            // alert('new loaded!');
        });
    } else if (!$(tabContainer).hasClass('active')) {

        // cached but not newly loaded!

        // show the tab
        $this.tab('show');
        // alert('cached!');
    } else {
        // already loaded and in view
        // alert('already loaded!');
    }

    return false;
});

var loadingDiv = $('#loadingDiv');

loadingDiv.hide();
// todo set to 0 width! every time
$(document).ajaxStart(function () {

    // show Loading Div
    loadingDiv.show(100);
    var progressBar = $('.progress-bar');
    progressBar.animate({
        width: "70%"
    }, 250).delay(500);
}).ajaxStop(function () {

    // hide loading div
    // loadingDiv.hide();
});


function remote_loader(tabContainer, URL, options) {

}